import {People} from './people';

export class Planet {
    name: string 
    rotation_period: string 
    orbital_period: string 
    diameter: string 
    climate: string 
    gravity: string 
    terrain: string
    surface_water: string 
    population: string 
    residents:People[]
 
    created: string
    edited: string
    url: string
}
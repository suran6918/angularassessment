import { People } from './people';

export class Species {
    name:string 
    classification:string
    designation:string 
    average_height:string 
    skin_colors:string 
    hair_colors:string 
    eye_colors:string 
    average_lifespan:string 
    homeworld:string 
    language:string 
    people: People[] 
    created:string 
    edited:string 
    url:string
    
}
import {Species} from './species';
import {Vehicles} from './vehicles';
import {Starships} from './starships';

export class People {
    name: string 
    height: string 
    mass: string
    hair_color: string 
    skin_color: string
    eye_color: string 
    birth_year: string 
    gender: string
    homeworld: string

    species: Species[] 
    vehicles: Vehicles[]
    starships: Starships[]
    created: string
    edited: string
    url: string
}
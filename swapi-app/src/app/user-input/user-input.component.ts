import { Component, OnInit } from '@angular/core';
import { GetCategoryService } from '../get-category.service';
import { ShareDataService } from '../share-data.service';


@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.component.html',
  styleUrls: ['./user-input.component.css']
})
export class UserInputComponent implements OnInit {

  allcategories
  categoryModel
  selectedCategory
  selectedID

  selectedName

  myObj: any;
  message:string;

  constructor(private myGetCategoryService: GetCategoryService, private data: ShareDataService) { }

  ngOnInit() {
    this.myGetCategoryService.getCategoryList().subscribe( (results)=>{
      this.allcategories = results
      console.log(this.allcategories)
      this.selectedCategory = results[0]
    })

    this.data.currentMessage.subscribe((message) => {
      this.message = message
    })
  }

  selectID(event: any){
    console.log('1.'+this.selectedCategory)
    console.log('2.'+this.selectedID)
    this.myGetCategoryService.getSelectedItem(this.selectedCategory, this.selectedID).subscribe( (result)=>{
      this.categoryModel = result

    })
  }

  selectItem(event: any) {
    this.selectedCategory = event.target.value;
  }


  passMessage(){
    this.data.changeMessage(this.categoryModel)
  }

}

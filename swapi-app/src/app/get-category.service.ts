import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetCategoryService {

  baseUrl = 'https://swapi.co/api/'
  finalUrl
  category
  id
  constructor(private http:HttpClient) { }

  getSelectedItem(c, i): Observable<any[]> {
    this.category = c
    this.id = i
    this.finalUrl = `${this.baseUrl}${this.category}/${this.id}`
    return this.http.get<any[]>(this.finalUrl)
  }

  getCategory(c):Observable<any[]> {
    this.category = c
    this.finalUrl = `${this.baseUrl}${this.category}`
    return this.http.get<any[]>(this.finalUrl)
  }

  getCategoryList():Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl)
  }

}

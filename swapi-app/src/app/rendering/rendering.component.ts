import { Component, OnInit } from '@angular/core';
import { UserInputComponent } from '../user-input/user-input.component';
import { ShareDataService } from '../share-data.service';

@Component({
  selector: 'app-rendering',
  templateUrl: './rendering.component.html',
  styleUrls: ['./rendering.component.css']
})
export class RenderingComponent implements OnInit {

  message: any;
  myKeys

  constructor(private data: ShareDataService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe((message) => {
      this.message = message
    })
  }

}
